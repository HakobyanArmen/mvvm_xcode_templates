#!/bin/bash
BASEDIR=$(dirname "$0")
#echo "$BASEDIR"

# Remove all existing files
rm -fR ~/Library/Developer/Xcode/Templates/My\ Templates/

# Create directory
mkdir -p ~/Library/Developer/Xcode/Templates/My\ Templates/

# Copy MVVM folder to the templates directory
cp -R $BASEDIR/MVVM ~/Library/Developer/Xcode/Templates/My\ Templates/
