//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class ___FILEBASENAMEASIDENTIFIER___: UIViewController {
        
    // MARK: - Initializers
    private let viewModel: ___VARIABLE_sceneName___ViewModelType
    init(viewModel: ___VARIABLE_sceneName___ViewModelType) {
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(.wrongInit)
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func bindTexts() {
    }
    
    override func bindUI() {
        
    }
    
    override func bindStyles() {
    }
    
    // MARK: - Binding
    private func bindViewModel() {
        // Outputs
        
        // Inputs
        
    }

}
